**This is a boiler plate of the leaseplan task r**

For this challenge I used webdriver.io because I have never did a real javascript project with automated testing, thus purely prototyping, but nonetheless, even if I do not get this freelance opportunity I learned a lot


I have setup a webdriver.io testscript based on the requested questions

Chronologically I picked the tasks up like this as it was easier for trial and error 
Task at hand which I did first: 

Create **automated scenarios** to check following functionality on:

1. https://www.leaseplan.com/en-be/business/showroom/ page:
2. cars filtering using checkboxes and range sliders
3. sorting by increasing/decreasing price after applying a filter
4. made a pageobject boiler plate,and embedded in first testscript
5. Create a bug report/reports for the page: https://www.leaseplan.com/nl-nl/stel-je-
vraag/
 



**Sync/Async**
 used the sync methology to build the **webdriver.io leaseplan.js** test script

 
 Tasks done: 

1. Get the job results from gitlab to bitbucket implement **shell script** (done)
2. cars filterering sorting from https://www.leaseplan.com/en-be/business/showroom/ page
2. Setup **page objects* boiler plate -done,  (done, partly to show I can do it)
3. parrallel running firefox and chrome(done). used two config files for this (done, quick way) but can also use an import of capabilities in wdio.conf. based on the selection of the browser)
4. pageobject boilerplate (done)

Todo to learn for myself with wdio
1. Reporting of execution + video (-not done set up in my allure branch) (to do, no time in my personal and business life :-) for now using spec headless solution) 
2. reusing testsuite.xml output (allure report) store them S3/google cloud bucket and upload it to netlify as well as allure framework to fetch history (todo :seperate project, own extra challenge)
3. Integrate with **AWS device farm** to run the script on firefox, chrome, and android (todo: separae project, own extra challenge)

